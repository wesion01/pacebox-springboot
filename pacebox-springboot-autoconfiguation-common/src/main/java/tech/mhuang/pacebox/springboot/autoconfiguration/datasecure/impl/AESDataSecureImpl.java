package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.impl;

import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.AbstractDataSecure;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.util.CryptoUtil;

/**
 * AES 默认实现
 *
 * @author mhuang
 * @since 1.0.0
 */
public class AESDataSecureImpl extends AbstractDataSecure {

    @Override
    public String encrypt(String content, String pwd) throws Exception {
        return CryptoUtil.encrypt(content, pwd, CryptoUtil.AES, getEncryptCoding());
    }

    @Override
    public String decrypt(String content, String pwd) throws Exception {
        return CryptoUtil.decrypt(content, pwd, CryptoUtil.AES, getDecryptCoding());
    }
}
