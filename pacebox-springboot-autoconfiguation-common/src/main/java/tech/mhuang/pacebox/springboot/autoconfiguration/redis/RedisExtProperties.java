package tech.mhuang.pacebox.springboot.autoconfiguration.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

@Data
@ConfigurationProperties(prefix = ConfigConsts.REDIS)
public class RedisExtProperties {

    private boolean enable;
}
