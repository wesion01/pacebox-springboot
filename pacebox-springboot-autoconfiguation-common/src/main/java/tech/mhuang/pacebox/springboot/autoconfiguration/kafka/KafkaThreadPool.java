package tech.mhuang.pacebox.springboot.autoconfiguration.kafka;

import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.core.spring.pool.SpringThreadPool;

/**
 * kafka 线程池
 *
 * @author mhuang
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = ConfigConsts.KAFKA_POOL)
public class KafkaThreadPool extends SpringThreadPool {

    private final String DEFAULT_NAME = "kafkaThreadPool";

    public KafkaThreadPool() {
        super();
        setBeanName(DEFAULT_NAME);
    }
}