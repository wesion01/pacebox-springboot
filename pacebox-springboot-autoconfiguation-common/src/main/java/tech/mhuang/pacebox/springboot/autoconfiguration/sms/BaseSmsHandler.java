package tech.mhuang.pacebox.springboot.autoconfiguration.sms;

import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendResult;

/**
 * 短信接口
 *
 * @author mhuang
 * @since 1.0.7
 */
public interface BaseSmsHandler {

    void setFieldProperties(SmsFieldProperties properties);
    /**
     * 短信发送
     *
     * @param smsBean 短信发送实体
     * @return 短信发送结果
     */
    SmsSendResult send(SmsSendRequest smsBean);
}