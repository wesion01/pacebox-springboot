package tech.mhuang.pacebox.springboot.autoconfiguration.kafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.core.check.CheckAssert;
import tech.mhuang.pacebox.kafka.admin.KafkaFramework;
import tech.mhuang.pacebox.kafka.admin.external.IKafkaExternal;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;

/**
 * kafka配置类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(KafkaFramework.class)
@EnableConfigurationProperties(value = {KafkaProperties.class, KafkaThreadPool.class})
@ConditionalOnProperty(prefix = ConfigConsts.KAFKA, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
@Slf4j
public class KafkaAutoConfiguration {

    private final KafkaProperties kafkaProperties;
    private final KafkaThreadPool kafkaThreadPool;

    public KafkaAutoConfiguration(KafkaProperties kafkaProperties,
                                  KafkaThreadPool kafkaThreadPool) {
        this.kafkaProperties = kafkaProperties;
        this.kafkaThreadPool = kafkaThreadPool;
    }

    @Bean
    @ConditionalOnMissingBean
    public IKafkaExternal springKafkaExternal() {
        return new SpringKafkaExternal();
    }

    @Bean
    @ConditionalOnMissingBean
    public KafkaFramework kafkaFramework(IKafkaExternal kafkaExternal, SpringContextHolder springContextHolder) {
        CheckAssert.check(springContextHolder, "SpringContextHolder不存在");
        CheckAssert.check(this.kafkaProperties, "KafkaProperties不存在");
        KafkaFramework framework = new KafkaFramework(this.kafkaProperties);
        kafkaThreadPool.initialize();
        framework.executorService(this.kafkaThreadPool);
        framework.kafkaExternal(kafkaExternal);
        framework.start();
        return framework;
    }
}
