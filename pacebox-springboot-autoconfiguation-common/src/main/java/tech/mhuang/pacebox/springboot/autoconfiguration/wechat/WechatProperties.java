package tech.mhuang.pacebox.springboot.autoconfiguration.wechat;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * task properties
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.WECHAT)
public class WechatProperties {

    /**
     * default task enable is <code>false</code>
     */
    private boolean enable;
}
