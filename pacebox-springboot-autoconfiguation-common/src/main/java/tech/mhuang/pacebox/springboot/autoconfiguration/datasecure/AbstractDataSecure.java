package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure;

import lombok.Getter;
import lombok.Setter;

import java.nio.charset.Charset;

/**
 * 加解密数据抽象（可自行实现）
 *
 * @author mhuang
 * @since 1.0.0
 */
public abstract class AbstractDataSecure {

    /**
     * 加密编码
     */
    @Setter
    @Getter
    Charset encryptCoding = Charset.defaultCharset();

    /**
     * 解密编码
     */
    @Setter
    @Getter
    Charset decryptCoding = Charset.defaultCharset();

    /**
     * 数据加密
     *
     * @param content 原文
     * @param pwd     公钥
     * @return 返回加密密文
     * @throws Exception 异常
     */
    public abstract String encrypt(String content, String pwd) throws Exception;

    /**
     * 数据解密
     *
     * @param content 密文
     * @param pwd     私钥
     * @return 返回原文
     * @throws Exception 异常
     */
    public abstract String decrypt(String content, String pwd) throws Exception;
}
