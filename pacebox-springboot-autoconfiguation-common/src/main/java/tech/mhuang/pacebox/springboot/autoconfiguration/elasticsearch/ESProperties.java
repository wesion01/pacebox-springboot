package tech.mhuang.pacebox.springboot.autoconfiguration.elasticsearch;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.elasticsearch.admin.bean.ESInfo;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * elasticsearch属性配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.ELASTICSEARCH)
public class ESProperties extends ESInfo {

    /**
     * auto open elasticsearch enable.
     * default enable false
     */
    private boolean enable;
}
