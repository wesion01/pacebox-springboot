package tech.mhuang.pacebox.springboot.autoconfiguration.trace.mybatis;

import io.opentracing.Tracer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis 配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@EnableConfigurationProperties(value = TraceMybatisProperties.class)
@ConditionalOnProperty(prefix = TraceMybatisProperties.PROP, name = "enable", havingValue = "true", matchIfMissing = true)
public class TraceMybatisConfiguration {
    public final Tracer tracer;

    public TraceMybatisConfiguration(Tracer tracer) {
        this.tracer = tracer;
    }

    @Bean
    public TraceMybatisInterceptor traceMybatisInterceptor() {
        return new TraceMybatisInterceptor(tracer);
    }
}