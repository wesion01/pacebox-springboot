package tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms;

import io.opentracing.Tracer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * 短信埋点
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@EnableConfigurationProperties(value = TraceSmsProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.TRACE_SMS, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE, matchIfMissing = true)
public class TraceSmsConfiguration {

    public final Tracer tracer;

    public TraceSmsConfiguration(Tracer tracer) {
        this.tracer = tracer;
    }

    @Bean
    public TraceSmsTemplateInteceptor traceSmsTemplateInteceptor(){
        return new TraceSmsTemplateInteceptor(tracer);
    }
}
