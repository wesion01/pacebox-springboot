package tech.mhuang.pacebox.springboot.autoconfiguration.trace;

import io.opentracing.contrib.spring.tracer.configuration.TracerAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.mybatis.TraceMybatisConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.rest.TraceRestTemplateConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.servlet.TraceServletConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms.TraceSmsConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms.TraceSmsTemplateInteceptor;

/**
 * 自动Trace注入
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@Import({TraceMybatisConfiguration.class, TraceRestTemplateConfiguration.class, TraceServletConfiguration.class, TraceSmsConfiguration.class})
@EnableConfigurationProperties(value = TraceProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.TRACE, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
@AutoConfigureAfter(TracerAutoConfiguration.class)
public class TraceAutoConfiguration {


}
