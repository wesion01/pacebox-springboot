package tech.mhuang.pacebox.springboot.autoconfiguration.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.core.check.CheckAssert;
import tech.mhuang.pacebox.jwt.admin.JwtFramework;
import tech.mhuang.pacebox.jwt.admin.external.IJwtExternal;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.autoconfiguration.SpringBootExtAutoConfiguration;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;

/**
 * jwt构建类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(JwtFramework.class)
@EnableConfigurationProperties(JwtProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.JWT, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
@AutoConfigureAfter({SpringBootExtAutoConfiguration.class})
@Slf4j
public class JwtAutoConfiguration {

    private final JwtProperties properties;

    public JwtAutoConfiguration(JwtProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean
    public IJwtExternal jwtExternal() {
        return new SpringJwtExternal();
    }

    @Bean
    @ConditionalOnMissingBean
    public JwtFramework framework(IJwtExternal jwtExternal, SpringContextHolder springContextHolder) {
        CheckAssert.check(this.properties, "jwt properties invalid...");
        CheckAssert.check(springContextHolder, "SpringContextHolder不存在、请设置mhuang.holder.enable=true");
        JwtFramework framework = new JwtFramework(this.properties);
        framework.external(jwtExternal);
        framework.start();
        return framework;
    }
}
