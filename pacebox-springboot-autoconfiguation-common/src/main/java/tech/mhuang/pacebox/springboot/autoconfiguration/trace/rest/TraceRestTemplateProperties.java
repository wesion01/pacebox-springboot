package tech.mhuang.pacebox.springboot.autoconfiguration.trace.rest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * RestTemplate 配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = TraceRestTemplateProperties.PROP)
public class TraceRestTemplateProperties {
    public static final String PROP = "pacebox.trace.rest";
    private boolean enable = Boolean.TRUE;
}