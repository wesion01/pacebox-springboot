package tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

@Data
@ConfigurationProperties(prefix = ConfigConsts.TRACE_SMS)
public class TraceSmsProperties {
    private boolean enable;
}
