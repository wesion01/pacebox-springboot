package tech.mhuang.pacebox.springboot.autoconfiguration.trace.servlet;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = TraceServletProperties.PROP)
public class TraceServletProperties {
    static final String PROP = "pacebox.trace.servlet";
    private boolean enable = Boolean.TRUE;
}
