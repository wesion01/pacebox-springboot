package tech.mhuang.pacebox.springboot.autoconfiguration.trace.sms;

import com.alibaba.fastjson.JSON;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.tag.Tags;
import lombok.extern.slf4j.Slf4j;
import tech.mhuang.pacebox.core.dict.BasicDict;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.SmsSendInterceptor;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendResult;

/**
 * 短信埋点
 *
 * @author mhuang
 * @since 1.0.0
 */
@Slf4j
public class TraceSmsTemplateInteceptor implements SmsSendInterceptor {

    private final Tracer tracer;

    private ThreadLocal<Span> spanThreadLocal = ThreadLocal.withInitial(() -> null);
    public TraceSmsTemplateInteceptor(Tracer tracer){
        this.tracer = tracer;
    }
    @Override
    public void onRequest(SmsSendRequest data) {
        Span span = tracer.buildSpan(data.getType())
                .withTag(Tags.SPAN_KIND.getKey(), Tags.SPAN_KIND_CLIENT)
                .start();
        Tags.COMPONENT.set(span, "sms");
        span.setTag("request.body", JSON.toJSONString(data));
        spanThreadLocal.set(span);
    }

    @Override
    public void onResponse(SmsSendResult data) {
        Span span = spanThreadLocal.get();
        span.setTag("response.body",JSON.toJSONString(data));
        span.finish();
        spanThreadLocal.remove();
    }

    @Override
    public void onError(Throwable e) {
        Span span = spanThreadLocal.get();
        Tags.ERROR.set(span,true);
        StackTraceElement[] stes = e.getStackTrace();
        StringBuilder errDetailMsg = new StringBuilder();
        if (stes != null && stes.length > 0) {
            for (StackTraceElement ste : stes) {
                errDetailMsg.append(ste.toString());
                errDetailMsg.append(System.getProperty("line.separator"));
            }
        }
        BasicDict basicDict = new BasicDict();
        basicDict.set("event", Tags.ERROR.getKey()).set("error.object", e).set("error.detail", errDetailMsg);
        span.log(basicDict);
        span.finish();
        spanThreadLocal.remove();
    }
}
