package tech.mhuang.pacebox.springboot.autoconfiguration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tech.mhuang.pacebox.core.id.BaseIdeable;
import tech.mhuang.pacebox.core.id.SnowflakeIdeable;
import tech.mhuang.pacebox.springboot.autoconfiguration.auth.AuthAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.DataSecureAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.elasticsearch.ESAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.exception.ExceptionAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.jackson.InterJacksonAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.jwt.JwtAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.kafka.KafkaAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.redis.RedisExtAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.rest.RestAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.SmsAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.swagger.GatewaySwaggerAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.swagger.SwaggerAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.task.TaskAutoConfiguration;
import tech.mhuang.pacebox.springboot.autoconfiguration.trace.TraceAutoConfiguration;
import tech.mhuang.pacebox.springboot.core.servlet.RequestFilter;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;

/**
 * 自动配置注入包.
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@Import({AuthAutoConfiguration.class, RestAutoConfiguration.class,
        SwaggerAutoConfiguration.class, RedisExtAutoConfiguration.class,
        TaskAutoConfiguration.class, DataSecureAutoConfiguration.class,
        InterJacksonAutoConfiguration.class, JwtAutoConfiguration.class,
        ESAutoConfiguration.class, GatewaySwaggerAutoConfiguration.class,
        KafkaAutoConfiguration.class, TraceAutoConfiguration.class,
        SmsAutoConfiguration.class,
        ExceptionAutoConfiguration.class})
@EnableConfigurationProperties(value = SpringBootExtProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.SPRINGBOOT, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
public class SpringBootExtAutoConfiguration {

    @Bean
    public FilterRegistrationBean<RequestFilter> requestRegisterBean() {
        RequestFilter requestFilter = new RequestFilter();
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(requestFilter);
        registration.setOrder(1);
        registration.addUrlPatterns("/*");
        registration.setName("RequestFilter");
        return registration;
    }

    /**
     * create context process tool
     *
     * @return SpringContextHolder
     */
    @Bean
    @ConditionalOnMissingBean
    public SpringContextHolder contextHolder() {
        return new SpringContextHolder();
    }

    /**
     * create generator id.
     * default used snowflake
     *
     * @return BaseIdeable
     */
    @Bean
    @ConditionalOnMissingBean
    public BaseIdeable snowflake() {
        return new SnowflakeIdeable();
    }
}