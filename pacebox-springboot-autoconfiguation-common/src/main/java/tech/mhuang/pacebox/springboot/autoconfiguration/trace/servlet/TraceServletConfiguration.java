package tech.mhuang.pacebox.springboot.autoconfiguration.trace.servlet;

import io.opentracing.contrib.web.servlet.filter.ServletFilterSpanDecorator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * servlet配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@EnableConfigurationProperties(value = TraceServletProperties.class)
@ConditionalOnProperty(prefix = TraceServletProperties.PROP, name = "enable", havingValue = "true", matchIfMissing = true)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class TraceServletConfiguration {

    @Bean
    public ServletFilterSpanDecorator servletFilterSpanDecorator() {
        return new TraceServletFilterSpanDecorator();
    }


}
