package tech.mhuang.pacebox.springboot.autoconfiguration.sms;

import tech.mhuang.pacebox.core.interceptor.BaseInterceptor;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendResult;

/**
 * 短信发送接口拦截
 *
 * @author mhuang
 * @since 1.0.7
 */
public interface SmsSendInterceptor extends BaseInterceptor<SmsSendRequest, SmsSendResult> {

}