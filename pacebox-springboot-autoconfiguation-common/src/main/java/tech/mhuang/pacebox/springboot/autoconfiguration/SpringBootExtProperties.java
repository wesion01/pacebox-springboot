package tech.mhuang.pacebox.springboot.autoconfiguration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * SpringBoot扩展配置
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.SPRINGBOOT)
public class SpringBootExtProperties {


    private boolean enable;
}