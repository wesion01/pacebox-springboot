package tech.mhuang.pacebox.springboot.autoconfiguration;

public class ConfigConsts {
    public static final String PROP = "pacebox.";
    public static final String SPRINGBOOT = PROP + "springboot";
    public static final String AUTH = PROP + "auth";
    public static final String REST = PROP + "rest";
    public static final String REST_SINGLE = REST + ".single";
    public static final String REST_MICRO = REST + ".micro";
    public static final String DATA_SECURE = PROP + "data-secure";
    public static final String ELASTICSEARCH = PROP + "elasticsearch";
    public static final String JWT = PROP + "jwt";
    public static final String KAFKA = PROP + "kafka";
    public static final String KAFKA_POOL = KAFKA + ".pool";
    public static final String REDIS = PROP + "redis";
    public static final String SWAGGER = PROP + "swagger";
    public static final String TASK = PROP + "task";

    public static final String WECHAT = PROP + "wechat";
    public static final String WECHAT_POOL = WECHAT + ".pool";
    public static final String SMS = PROP + "sms";

    public static final String ENABLE = "enable";
    public static final String TRACE = PROP + "trace";
    public static final String TRACE_SMS = TRACE + ".sms";
    public static final String TRUE = "true";
}
