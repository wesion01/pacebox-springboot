package tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.SmsFieldProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * 短信配置参数
 *
 * @author mhuang
 * @since 1.0.7
 */
@Data
@ConfigurationProperties(prefix = ConfigConsts.SMS)
public class SmsProperties {

    private boolean enable;

    /**
     * 需要配置的bean
     */
    private Map<String, SmsFieldProperties> beanMap = new HashMap<>();
}
