package tech.mhuang.pacebox.springboot.autoconfiguration.trace.mybatis;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = TraceMybatisProperties.PROP)
public class TraceMybatisProperties {
    static final String PROP = "pacebox.trace.mybatis";
    private boolean enable = Boolean.TRUE;
}