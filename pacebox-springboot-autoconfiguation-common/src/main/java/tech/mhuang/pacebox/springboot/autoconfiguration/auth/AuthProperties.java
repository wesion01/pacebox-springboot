package tech.mhuang.pacebox.springboot.autoconfiguration.auth;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 权限校验
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.AUTH)
public class AuthProperties {

    /**
     * default auth is <code>false</code>
     */
    private boolean enable;

    /**
     * default auth redisdatabase is <code>0</code>
     */
    private Integer redisDataBase = 0;

    /**
     * default not check url
     */
    private boolean checkInterceptorUrl;

    /**
     * interceptor include url
     */
    private List<String> interceptorIncludeUrl = new ArrayList<>();

    /**
     * interceptor exclude url
     */
    private List<String> interceptorExcludeUrl = new ArrayList<>();

    /**
     * filter include url
     */
    private List<String> filterIncludeUrl = Stream.of("/*").collect(Collectors.toList());

    /**
     * filter default authType
     */
    private String filterDefAuthType;

    /**
     * security include url
     */
    private List<String> securityIncludeUrl = Stream.of("/monitor/**").collect(Collectors.toList());
}