package tech.mhuang.pacebox.springboot.autoconfiguration.jwt;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.jwt.admin.bean.Jwt;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * jwt属性类
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.JWT)
public class JwtProperties extends Jwt {

    /**
     * auto open jwt enable.
     * default enable true
     */
    private boolean enable;

    public JwtProperties() {
        super();
        enable = true;
    }
}
