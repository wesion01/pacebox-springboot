package tech.mhuang.pacebox.springboot.autoconfiguration.sms.aliyun;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.HttpClientConfig;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import lombok.extern.slf4j.Slf4j;
import tech.mhuang.pacebox.core.exception.ExceptionUtil;
import tech.mhuang.pacebox.core.util.StringUtil;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.BaseSmsHandler;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.SmsFieldProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendResult;

/**
 * 阿里云短信
 *
 * @author mhuang
 * @since 1.0.7
 */
@Slf4j
public class AliyunSmsHandler implements BaseSmsHandler {

    private IAcsClient acsClient;
    private SmsFieldProperties properties;


    @Override
    public void setFieldProperties(SmsFieldProperties properties) {
        this.properties = properties;
        DefaultProfile profile = DefaultProfile.getProfile(properties.getRegion(), properties.getAccessKey(), properties.getAccessSecret());
        if (properties.isUseProxy()) {
            HttpClientConfig httpClientConfig = profile.getHttpClientConfig();
            httpClientConfig.setHttpProxy("http://" + properties.getProxyIp() + ":" + properties.getProxyPort());
            httpClientConfig.setHttpsProxy("https://" + properties.getProxyIp() + ":" + properties.getProxyPort());
        }
        this.acsClient = new DefaultAcsClient(profile);
    }

    @Override
    public SmsSendResult send(SmsSendRequest smsSendRequest) {
        SmsSendResult smsResult = SmsSendResult.builder().extendParam(smsSendRequest.getExtendParam()).build();
        CommonRequest request = getCommonRequest(AliyunSmsConsts.Action.SEND);
        try {
            request.putQueryParameter("PhoneNumbers", smsSendRequest.getMobile());
            request.putQueryParameter("SignName", smsSendRequest.getSignName());
            request.putQueryParameter("TemplateCode", smsSendRequest.getAliTemplateCode());
            request.putQueryParameter("TemplateParam", JSON.toJSONString(smsSendRequest.getTemplateParam()));
            CommonResponse commonResponse = acsClient.getCommonResponse(request);
            JSONObject rstJson = JSON.parseObject(commonResponse.getData());
            String message = rstJson.getString("Message");
            if (StringUtil.equals(rstJson.getString("Code"), "OK")) {
                smsResult.setSuccess(true);
                smsResult.setBizId(rstJson.getString("BizId"));
                smsResult.setRequestId(rstJson.getString("RequestId"));
            } else {
                smsResult.setMessage(message);
            }
        } catch (ClientException e) {
            smsResult.setMessage(ExceptionUtil.getMessage(e));
            smsResult.setThrowable(e);
        }
        return smsResult;
    }

    /**
     * /**
     * 获取通用请求
     *
     * @return 通用请求
     */
    private CommonRequest getCommonRequest(AliyunSmsConsts.Action action) {
        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(AliyunSmsConsts.DOMAIN);
        request.setSysVersion(AliyunSmsConsts.VERSION);
        request.setSysAction(action.value);
        return request;
    }
}