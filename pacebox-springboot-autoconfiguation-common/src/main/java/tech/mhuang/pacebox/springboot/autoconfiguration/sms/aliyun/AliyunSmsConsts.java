package tech.mhuang.pacebox.springboot.autoconfiguration.sms.aliyun;

/**
 * 阿里云短信常量类
 *
 * @author mhuang
 * @since 1.0.7
 */
public class AliyunSmsConsts {

    public static final String DOMAIN = "dysmsapi.aliyuncs.com";
    public static final String VERSION = "2017-05-25";

    /**
     * 动作
     */
    public enum Action {
        /**
         * 短信发送
         */
        SEND("SendSms"),

        /**
         * 短信批量发送
         */
        SEND_BATCH("SendBatchSms");

        String value;

        Action(String value) {
            this.value = value;
        }
    }
}
