package tech.mhuang.pacebox.springboot.autoconfiguration.sms;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import tech.mhuang.pacebox.core.util.ObjectUtil;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendRequest;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsSendResult;
import tech.mhuang.pacebox.springboot.core.exception.BusinessException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 短信模板
 *
 * @author mhuang
 * @since 1.0.7
 */
@Slf4j
public class SmsTemplate implements SmsOperation {

    /**
     * 具体配置了多少个短信处理器(发送时进行指定）
     */
    private Map<String, BaseSmsHandler> smsHandlerMap = new ConcurrentHashMap<>();

    /**
     * 拦截器（用于在消息开始和结束、异常前改变消息、以及可用于短信埋点）
     */
    private List<SmsSendInterceptor> interceptors = new ArrayList<>();

    @Override
    public void addHandler(String key, BaseSmsHandler smsHandler) {
        smsHandlerMap.put(key, smsHandler);
    }

    @Override
    public void addInteceptor(SmsSendInterceptor sendInterceptor) {
        interceptors.add(sendInterceptor);
    }

    public void setInterceptors(List<SmsSendInterceptor> interceptors) {
        this.interceptors = interceptors;
    }

    @Override
    public SmsSendResult send(SmsSendRequest smsSendRequest) {
        log.info("开始发送短信====request:{}", JSON.toJSONString(smsSendRequest));
        for (SmsSendInterceptor interceptor : interceptors) {
            interceptor.onRequest(smsSendRequest);
        }
        SmsSendResult result;
        BaseSmsHandler smsHandler = smsHandlerMap.get(smsSendRequest.getType());
        if (ObjectUtil.isEmpty(smsHandler)) {
            result = SmsSendResult.builder().success(false)
                    .message("找不到发送的短信配置")
                    .throwable(new BusinessException(500, "找不到发送的短信配置")).build();
        } else {
            result = smsHandler.send(smsSendRequest);
        }
        if (ObjectUtil.isEmpty(result.getThrowable())) {
            for (SmsSendInterceptor interceptor : interceptors) {
                interceptor.onResponse(result);
            }
        } else {
            for (SmsSendInterceptor interceptor : interceptors) {
                interceptor.onError(result.getThrowable());
            }
        }
        log.info("短信发送结果====request:{},response:{}", JSON.toJSONString(smsSendRequest), JSON.toJSONString(result));
        return result;
    }
}