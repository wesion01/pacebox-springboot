package tech.mhuang.pacebox.springboot.autoconfiguration.jackson;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * jackson配置、去掉引号
 *
 * @author mhuang
 * @since 1.0.0
 */
@Configuration
@ConditionalOnClass(JacksonAutoConfiguration.class)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class InterJacksonAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public JacksonConfigurer jacksonConfigurer() {
        return new JacksonConfigurer();
    }
}