package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.annation;

import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.AbstractDataSecure;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.impl.AESDataSecureImpl;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据安全字段处理（用于对应MappingType为Field中）
 *
 * @author mhuang
 * @since 1.0.0
 */
@Target(value = {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSecureField {

    /**
     * 是否加密
     *
     * @return 默认True
     */
    boolean encode() default true;

    /**
     * 是否解密
     *
     * @return 默认True
     */
    boolean decode() default true;

    /**
     * 加密默认处理类为AES
     *
     * @return 可自行实现
     */
    Class<? extends AbstractDataSecure> Clazz() default AESDataSecureImpl.class;
}
