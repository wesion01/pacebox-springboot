package tech.mhuang.pacebox.springboot.autoconfiguration.task;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;

/**
 * task properties
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = ConfigConsts.TASK)
public class TaskProperties {

    /**
     * default task enable is <code>FALSE</code>
     */
    private boolean enable;

    /**
     * default name is <code>defaultInterTask</code>
     */
    private String name = "defaultInterTask";

    /**
     * default pool is <code>50</code>
     */
    private int poolSize = 50;
}
