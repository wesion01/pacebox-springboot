package tech.mhuang.pacebox.springboot.autoconfiguration.sms;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * 短信字段属性
 *
 * @author mhuang
 * @since 1.0.0
 */
@Data
@Builder
public class SmsFieldProperties {

    @Tolerate
    public SmsFieldProperties(){

    }

    /**
     * 短信驱动
     */
    private String driverName;

    /**
     * 区域
     */
    private String region;

    /**
     * accessKey
     */
    private String accessKey;

    /**
     * accessSecret
     */
    private String accessSecret;

    /**
     * 是否使用代理
     */
    private boolean useProxy;

    /**
     * 代理ip
     */
    private String proxyIp;

    /**
     * 代理端口
     */
    private String proxyPort;
}
