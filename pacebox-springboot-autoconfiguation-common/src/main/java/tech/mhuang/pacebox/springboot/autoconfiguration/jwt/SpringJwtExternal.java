package tech.mhuang.pacebox.springboot.autoconfiguration.jwt;

import tech.mhuang.pacebox.jwt.admin.JwtHelper;
import tech.mhuang.pacebox.jwt.admin.external.IJwtExternal;
import tech.mhuang.pacebox.jwt.admin.external.IJwtProducer;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;

/**
 * jwt spring实现
 *
 * @author mhuang
 * @since 1.0.0
 */
public class SpringJwtExternal implements IJwtExternal {

    @Override
    public IJwtProducer create(String key) {
        return SpringContextHolder.registerBean(key, JwtHelper.class);
    }
}