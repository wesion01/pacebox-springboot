package tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tech.mhuang.pacebox.core.dict.BasicDict;

import java.util.Map;

/**
 * 短信发送工具类
 *
 * @author mhuang
 * @since 1.0.7
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SmsSendRequest {

    String type;
    /**
     * 手机号
     */
    String mobile;

    /**
     * 消息内容
     */
    String content;

    /**
     * 签名
     */
    String signName;

    /**
     * 替换的模板code
     */
    String aliTemplateCode;

    /**
     * 模板参数
     */
    BasicDict templateParam;

    /**
     * 扩展参数、用于自行业务处理(采用回调时会带入)
     */
    BasicDict extendParam;
}