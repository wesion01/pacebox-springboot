package tech.mhuang.pacebox.springboot.autoconfiguration.sms;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.mhuang.pacebox.springboot.autoconfiguration.ConfigConsts;
import tech.mhuang.pacebox.springboot.autoconfiguration.sms.domain.SmsProperties;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 短信配置
 *
 * @author mhuang
 * @since 1.0.7
 */
@Configuration
@ConditionalOnClass(SmsOperation.class)
@EnableConfigurationProperties(value = SmsProperties.class)
@ConditionalOnProperty(prefix = ConfigConsts.SMS, name = ConfigConsts.ENABLE, havingValue = ConfigConsts.TRUE)
@Slf4j
public class SmsAutoConfiguration {

    private final SmsProperties properties;

    public SmsAutoConfiguration(SmsProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean
    public SmsTemplate smsTemplate() {
        SmsTemplate smsTemplate = new SmsTemplate();
        configure(smsTemplate);
        return smsTemplate;
    }

    /**
     * 配置短信模板
     * @param smsTemplate 短信模板
     */
    private void configure(SmsTemplate smsTemplate) {
        Map<String, SmsFieldProperties> smsHandlerMap = properties.getBeanMap();
        Set<String> keys = smsHandlerMap.keySet();
        for(String key : keys){
            SmsFieldProperties fieldProperties = smsHandlerMap.get(key);
            try {
                BaseSmsHandler smsHandler = (BaseSmsHandler) Class.forName(fieldProperties.getDriverName()).newInstance();
                smsHandler.setFieldProperties(smsHandlerMap.get(key));
                smsTemplate.addHandler(key, smsHandler);
            } catch (InstantiationException e) {
                log.error("获取短信处理类失败",e);
            } catch (IllegalAccessException e) {
                log.error("获取短信处理类失败",e);
            } catch (ClassNotFoundException e) {
                log.error("找不到短信处理类",e);
            }
        }
    }

    /**
     * 短信模板处理配置
     *
     * @author mhuang
     * @since 1.0.0
     */
    @Configuration
    public static class SmsTemplatePostProcessingConfiguration {

        private final SmsTemplate smsTemplate;
        private final List<SmsSendInterceptor> smsSendInterceptors;

        public SmsTemplatePostProcessingConfiguration(SmsTemplate smsTemplate, List<SmsSendInterceptor> smsSendInterceptors) {
            this.smsTemplate = smsTemplate;
            this.smsSendInterceptors = smsSendInterceptors;
        }

        /**
         * 添加拦截器
         */
        @PostConstruct
        public void init() {
            smsTemplate.setInterceptors(this.smsSendInterceptors);
        }
    }
}
