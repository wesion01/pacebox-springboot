package tech.mhuang.pacebox.springboot.autoconfiguration.kafka;

import tech.mhuang.pacebox.kafka.admin.external.IKafkaConsumer;
import tech.mhuang.pacebox.kafka.admin.external.IKafkaExternal;
import tech.mhuang.pacebox.kafka.admin.external.IKafkaProducer;
import tech.mhuang.pacebox.kafka.consumer.process.DefaultKafkaConsumer;
import tech.mhuang.pacebox.kafka.producer.process.DefaultKafkaProducer;
import tech.mhuang.pacebox.springboot.core.spring.reflect.SpringReflectInvoke;
import tech.mhuang.pacebox.springboot.core.spring.start.SpringContextHolder;

/**
 * spring 实现kafka动态创建对应bean
 *
 * @author mhuang
 * @since 1.0.0
 */
public class SpringKafkaExternal implements IKafkaExternal {

    /**
     * 创建生产者
     *
     * @param key 产生的key
     * @return kafka生产者
     */
    @Override
    public IKafkaProducer createProducer(String key) {
        return SpringContextHolder.registerBean(key, DefaultKafkaProducer.class);
    }

    /**
     * 创建消费者
     *
     * @param key 产生的key
     * @return kafka消费者
     */
    @Override
    public IKafkaConsumer createConsumer(String key) {
        IKafkaConsumer kafkaConsumer = SpringContextHolder.registerBean(key, DefaultKafkaConsumer.class);
        kafkaConsumer.invoke(new SpringReflectInvoke());
        return kafkaConsumer;
    }
}
