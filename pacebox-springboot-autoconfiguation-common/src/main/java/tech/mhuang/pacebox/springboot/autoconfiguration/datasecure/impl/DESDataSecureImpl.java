package tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.impl;

import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.AbstractDataSecure;
import tech.mhuang.pacebox.springboot.autoconfiguration.datasecure.util.CryptoUtil;

/**
 * DES 默认实现
 *
 * @author mhuang
 * @since 1.0.0
 */
public class DESDataSecureImpl extends AbstractDataSecure {

    @Override
    public String encrypt(String content, String pwd) throws Exception {
        return CryptoUtil.encrypt(content, pwd, CryptoUtil.DES, getEncryptCoding());
    }

    @Override
    public String decrypt(String content, String pwd) throws Exception {
        return CryptoUtil.decrypt(content, pwd, CryptoUtil.DES, getDecryptCoding());
    }
}
