package tech.mhuang.pacebox.springboot.core.servlet;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        WebRequestHeader httpRequest = new WebRequestHeader(request);
        WebResponseHeader httpResponse = new WebResponseHeader(response);
        filterChain.doFilter(httpRequest, httpResponse);
    }
}
