package tech.mhuang.pacebox.springboot.core.servlet;

import tech.mhuang.pacebox.core.io.IOUtil;
import tech.mhuang.pacebox.core.util.StringUtil;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequestWrapper;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 重写HttpServletRequest、达到可添加Header、spring可重复读取数据封装
 *
 * @author mhuang
 * @since 1.0.0
 */
public class WebRequestHeader extends ServletRequestWrapper implements HttpServletRequest {

    //只有json提交时才获取流
    private byte[] body;
    private final Map<String, String> customHeaders;
    private HttpServletRequest request;

    public WebRequestHeader(HttpServletRequest request) throws IOException {
        super(request);
        this.request = request;
        this.customHeaders = new HashMap<>();
        this.body = toBody();
    }

    private final String BODY_CONTENT_TYPE = "application/json";

    private byte[] toBody() throws IOException {
        String contentType = request.getContentType();
        byte[] contentBody = new byte[0];
        if (StringUtil.isNotEmpty(contentType)) {
            contentType = contentType.split(";")[0];
            if (StringUtil.equals(contentType, BODY_CONTENT_TYPE)) {
                contentBody = IOUtil.toByteArray(request.getInputStream(), false);
            }
        }
        return contentBody;
    }

    /**
     * 扩展、放入Header
     *
     * @param name  放入header的key
     * @param value 放入header的value
     */
    public void putHeader(String name, String value) {
        this.customHeaders.put(name, value);
    }

    /**
     * 重构根据key获取header中的值
     *
     * @param name header的key
     * @return String 根据header的key得到的value
     */
    @Override
    public String getHeader(String name) {
        String headerValue = customHeaders.get(name);

        if (headerValue != null) {
            return headerValue;
        }
        return request.getHeader(name);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return request.getHeaderNames();
    }

    @Override
    public String getAuthType() {
        return request.getAuthType();
    }

    @Override
    public Cookie[] getCookies() {
        return request.getCookies();
    }

    @Override
    public long getDateHeader(String name) {
        return request.getDateHeader(name);
    }

    /**
     * 重构根据key获取header的值
     *
     * @param name header的key
     * @return Enumeration 根据header的key得到的header的value
     */
    @Override
    public Enumeration<String> getHeaders(String name) {
        if (customHeaders.containsKey(name)) {
            Set<String> em = new HashSet<>();
            em.add(customHeaders.get(name));
            return Collections.enumeration(em);
        }
        return request.getHeaders(name);
    }

    @Override
    public int getIntHeader(String name) {
        return request.getIntHeader(name);
    }

    @Override
    public String getMethod() {
        return request.getMethod();
    }

    @Override
    public String getPathInfo() {
        return request.getPathInfo();
    }

    @Override
    public String getPathTranslated() {
        return request.getPathTranslated();
    }

    @Override
    public String getContextPath() {
        return request.getContextPath();
    }

    @Override
    public String getQueryString() {
        return request.getQueryString();
    }

    @Override
    public String getRemoteUser() {
        return request.getRemoteUser();
    }

    @Override
    public boolean isUserInRole(String role) {
        return request.isUserInRole(role);
    }

    @Override
    public Principal getUserPrincipal() {
        return request.getUserPrincipal();
    }

    @Override
    public String getRequestedSessionId() {
        return request.getRequestedSessionId();
    }

    @Override
    public String getRequestURI() {
        return request.getRequestURI();
    }

    @Override
    public StringBuffer getRequestURL() {
        return request.getRequestURL();
    }

    @Override
    public String getServletPath() {
        return request.getServletPath();
    }

    @Override
    public HttpSession getSession(boolean create) {
        return request.getSession(create);
    }

    @Override
    public HttpSession getSession() {
        return request.getSession();
    }

    @Override
    public String changeSessionId() {
        return request.changeSessionId();
    }

    @Override
    public boolean isRequestedSessionIdValid() {
        return request.isRequestedSessionIdValid();
    }

    @Override
    public boolean isRequestedSessionIdFromCookie() {
        return request.isRequestedSessionIdFromCookie();
    }

    @Override
    public boolean isRequestedSessionIdFromURL() {
        return request.isRequestedSessionIdFromURL();
    }

    @Override
    @Deprecated
    public boolean isRequestedSessionIdFromUrl() {
        return request.isRequestedSessionIdFromURL();
    }

    @Override
    public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
        return request.authenticate(response);
    }

    @Override
    public void login(String username, String password) throws ServletException {
        request.login(username, password);
    }

    @Override
    public void logout() throws ServletException {
        request.logout();
    }

    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
        return request.getParts();
    }

    @Override
    public Part getPart(String name) throws IOException, ServletException {
        return request.getPart(name);
    }

    @Override
    public <T extends HttpUpgradeHandler> T upgrade(Class<T> httpUpgradeHandlerClass)
            throws IOException, ServletException {
        return request.upgrade(httpUpgradeHandlerClass);
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return new WebServletInputStream(this.body);
    }
}