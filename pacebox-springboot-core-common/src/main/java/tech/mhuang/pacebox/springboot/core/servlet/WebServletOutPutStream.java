package tech.mhuang.pacebox.springboot.core.servlet;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class WebServletOutPutStream extends ServletOutputStream {
    private ByteArrayOutputStream out;
    private HttpServletResponse response;

    public WebServletOutPutStream(HttpServletResponse response) {
        this.out = new ByteArrayOutputStream();
        this.response = response;
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setWriteListener(WriteListener writeListener) {

    }

    @Override
    public void write(int b) throws IOException {
        response.getOutputStream().write(b);
        out.write(b);
    }

    public byte[] toByteArray() {
        return out.toByteArray();
    }
}
