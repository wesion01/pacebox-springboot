package tech.mhuang.pacebox.springboot.core.servlet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/**
 * 输出结果检测
 *
 * @author mhuang
 * @since 1.0.2
 */
public class WebResponseHeader extends HttpServletResponseWrapper implements HttpServletResponse {

    private HttpServletResponse response;
    private WebServletOutPutStream buffer;

    /**
     * Constructs a response adaptor wrapping the given response.
     *
     * @param response the {@link HttpServletResponse} to be wrapped.
     * @throws IllegalArgumentException if the response is null
     */
    public WebResponseHeader(HttpServletResponse response) {
        super(response);
        this.response = response;
        this.buffer = new WebServletOutPutStream(response);
    }


    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return response.getOutputStream();
    }

    @Override
    public void flushBuffer() throws IOException {
        response.flushBuffer();
    }

    /**
     * 获取数据、可重复使用
     *
     * @return 字节
     * @throws IOException 异常
     * @since 1.0.2
     */
    public byte[] getContent() throws IOException {
        return buffer.toByteArray();
    }
}
