package tech.mhuang.pacebox.springboot.core.servlet;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class WebServletInputStream extends ServletInputStream {

    private final byte[] body;
    private final ByteArrayInputStream in;

    public WebServletInputStream(byte[] body) {
        this.body = body;
        this.in = new ByteArrayInputStream(body);
    }

    @Override
    public int read() throws IOException {
        return in.read();
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean isReady() {
        return false;
    }

    @Override
    public void setReadListener(ReadListener readListener) {

    }
}
