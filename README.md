<p align="center">
pacebox-springboot 基于pacebox & springboot 融合的工具包
</p>
<p align="center">
-- 主页：<a href="http://mhuang.tech/pacebox-springboot">http://mhuang.tech/pacebox-springboot</a>  --
</p>
<p align="center">
    -- QQ群①:<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=6703688b236038908f6c89b732758d00104b336a3a97bb511048d6fdc674ca01"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="pacebox官方交流群①" title="pacebox官方交流群①"></a>
</p>
---------------------------------------------------------------------------------------------------------------------------------------------------------

## 简介
pacebox-springboot 是一个基于pacebox & springboot融合的工具包、将pacebox以及springboot无缝接入
## 结构
```
    - interchan-auth-common     权限拦截器工具包
    - interchan-autoconfiguation springboot 自动注入工具包
        - jwt   
        - kafka 
        - elasticsearch
        - datasecure（数据保护）
        - swagger
        - wechat
    - interchan-core-common      核心处理工具包
    - interchan-payment-common   支付工具包
    - interchan-protocol-common  协议工具包
    - interchan-redis-common     基于redisTemplate简单封装工具包
    - interchan-redis-kafka-middle-common  kafka消费数据通过aop写入redis的工具包
    - interchan-starter-common   启动依赖工具包
    - interchan-wechat-common    微信封装工具包（包含小程序和微信公众号、原jwechat项目）
```

## 安装

### MAVEN
在pom.xml中加入
```
    <dependency>
        <groupId>tech.mhuang.pacebox</groupId>
        <artifactId>pacebox-springboot</artifactId>
        <version>${laster.version}</version>
    </dependency>
```
### 非MAVEN
下载任意链接
- [Maven中央库1](https://repo1.maven.org/maven2/tech/mhuang/pacebox/pacebox-springboot/)
- [Maven中央库2](http://repo2.maven.org/maven2/tech/mhuang/pacebox/pacebox-springboot/)

## demo案例

### springboot
[点击访问源码](http://gitee.com/pacebox/inter-boot-demo)
### springcloud
[点击访问源码](http://gitee.com/pacebox/inter-micro-demo)
> 注意
> pacebox只支持jdk1.8以上的版本