package tech.mhuang.pacebox.springboot.wechat.wechat.common.util;

import org.dom4j.Document;
import org.dom4j.Element;
import tech.mhuang.pacebox.core.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author mhuang
 */
public class XmlUtil {

    public static Map<String, Object> dom2Map(Document doc) {
        Map<String, Object> map = new HashMap<>();
        if (doc == null) {
            return map;
        }

        Element root = doc.getRootElement();
        for (Iterator iterator = root.elementIterator(); iterator.hasNext(); ) {
            Element e = (Element) iterator.next();
            List list = e.elements();
            if (list.size() > 0) {
                map.put(e.getName(), dom2Map(e));
            } else {
                map.put(e.getName(), e.getText());
            }

        }
        return map;
    }


    public static Map dom2Map(Element e) {
        Map map = new HashMap();
        List list = e.elements();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Element iter = (Element) list.get(i);
                List mapList = new ArrayList();

                if (iter.elements().size() > 0) {
                    Map m = dom2Map(iter);
                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());
                        if(StringUtil.equals(obj.getClass().getName(),"java.util.ArrayList")) {
                            mapList = (List) obj;
                            mapList.add(m);
                        }else{
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(m);
                        }
                        map.put(iter.getName(), mapList);
                    } else {
                        map.put(iter.getName(), m);
                    }
                } else {
                    if (map.get(iter.getName()) != null) {
                        Object obj = map.get(iter.getName());
                        if(StringUtil.equals(obj.getClass().getName(),"java.util.ArrayList")) {
                            mapList = (List) obj;
                            mapList.add(iter.getText());
                        }else{
                            mapList = new ArrayList();
                            mapList.add(obj);
                            mapList.add(iter.getText());
                        }
                        map.put(iter.getName(), mapList);
                    } else {
                        map.put(iter.getName(), iter.getText());
                    }
                }
            }
        } else {
            map.put(e.getName(), e.getText());
        }

        return map;
    }
}
