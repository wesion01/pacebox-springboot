package tech.mhuang.pacebox.springboot.wechat.wechat.common.model.message.child;

import lombok.Data;

@Data
public class Content {

    String content;

    public Content() {

    }
}
